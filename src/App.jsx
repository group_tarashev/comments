import React, { useEffect, useState } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import CommentList from "./components/CommentList";
import "./App.css";
import Input from "./components/Input";
import axios from "axios";
import Modal from "./components/Modal";
const App = () => {
  const clientQuery = new QueryClient();
  const [user, setUser] = useState({
    id: 3,
    user1: "juliusomo",
    img: "./images/avatars/image-juliusomo.png",
  });
  const [data, setData] = useState();
  const [isLoading, setIsloading] = useState(true);
  const [input, setInput] = useState();
  const [userId, setUserId] = useState(0);
  const [edit, setEdit] = useState({
    edit: false,
    id: null,
  });
  const [value, setValue] = useState("");
  const [modal, setModal] = useState(false);
  const [delId, setDelId] = useState(null);
  useEffect(() => {
    const request = async () => {
      return await axios.get("./comments.json").then((res) => {
        setData(res.data.data);
        setIsloading(false);
      });
    };
    request();
  }, []);
  const replay = (id) => {
    setUserId(id);
  };
  const handleInput = (e, img) => {
    e.preventDefault();
    setValue(e.target.value);
    setInput({
      id: Math.random() * 10,
      comment: e.target.value,
      user1: e.target.name,
      img: img,
      likes: 0,
      userLikes: [],
    });
  };
  const handleSubmit = (e, index) => {
    e.preventDefault();
    setData((prev) => [
      ...prev.slice(0, index + 1),
      input,
      ...prev.slice(index + 1),
    ]);
    replay(0);
    setValue("");
  };
  const handleSubmitComment = (e) => {
    e.preventDefault();
    setData((prev) => [...prev, input]);
  };
  const handleDelete = (id) => {
    setData((prev) => prev.filter((item) => item.id !== id));
  };
  const handleEdit = (id) => {
    setEdit({ edit: true, id: id });
  };
  const handleLikePlus = (id, itemId) => {
    setData((prev) => [
      ...prev.map((item) => {
        if (item.id === itemId && !item.userLikes.includes(id)) {
          return {
            ...item,
            likes: item.likes + 1,
            userLikes: [...item.userLikes, id],
          };
        }
        return item;
      }),
    ]);
  };
  const handleLikeMinus = (id, itemId) => {
    setData((prev) => [
      ...prev.map((item) => {
        if (item.id === itemId && !item.userLikes.includes(id)) {
          return {
            ...item,
            likes: item.likes - 1,
            userLikes: [...item.userLikes, id],
          };
        }
        return item;
      }),
    ]);
  };
  const handleModal = (id) => {
    setModal(!modal);
    setDelId(id)
  };
  return (
    <div className="App">
      <QueryClientProvider client={clientQuery}>
        <CommentList
          userId={userId}
          user={user}
          handleInput={handleInput}
          handleSubmit={handleSubmit}
          replay={replay}
          data={data}
          isLoading={isLoading}
          handleDelete={handleDelete}
          handleEdit={handleEdit}
          edit={edit}
          setEdit={setEdit}
          handleLikePlus={handleLikePlus}
          handleLikeMinus={handleLikeMinus}
          handleModal={handleModal}
        />
        {userId == 0 && (
          <Input
            user={user}
            handleInput={handleInput}
            handleSubmit={handleSubmitComment}
            value={value}
          />
        )}
        <Modal
          modal={modal}
          handleModal={handleModal}
          delId={delId}
          handleDelete={handleDelete}
        />
      </QueryClientProvider>
    </div>
  );
};

export default App;
