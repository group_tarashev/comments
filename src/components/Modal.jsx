import React from "react";
import "../styles/modal.css";
const Modal = ({ modal, handleModal, handleDelete, delId }) => {
  if (!modal) {
    return;
  }
  return (
    <div className="modal">
      <div className="modal-inner">
        <h2 className="modal-title">Do you whant to delete this message?</h2>
        <div className="btn-group-modal">
        <button onClick={() => { handleModal(delId); handleDelete(delId) }}>Delete</button>
        <button onClick={handleModal}>Cancel</button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
