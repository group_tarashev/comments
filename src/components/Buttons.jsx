import React from 'react'

const Buttons = ({item,handleLikePlus,id, itemId, handleLikeMinus}) => {
  return (
    <div className="com-likes">
    <button onClick={() => handleLikePlus(id, itemId)} className="btn-plus">+</button>
    <span>{item.likes}</span>
    <button onClick={()=> handleLikeMinus(id, itemId)} className="btn-minus">-</button>
  </div>
  )
}

export default Buttons