import React from "react";
import Buttons from "./Buttons";

const Comment = ({
  edit,
  item,
  repay,
  user,
  handleDelete,
  handleEdit,
  setEdit,
  handleLikePlus,
  handleLikeMinus,
  handleModal
}) => {
    const isUser = edit.edit && item.user1 === user.user1 && item.id === edit.id;
  return (
    <div className="comment">
      <Buttons item={item} handleLikePlus={handleLikePlus} itemId={item.id} id={user.user1} handleLikeMinus={handleLikeMinus}/>
      <div className="com-content">
        <div className="com-nav">
          <div className="avatar">
            <img src={item.img} alt="" />
            <span>{item.user1}</span>
          </div>
          {item.user1 !== user.user1 && (
            <div className="reply" onClick={() => repay(item.id)}>
              <img src="./images/icon-reply.svg" alt="" />
              <span>reply</span>
            </div>
          )}
          {item.user1 === user.user1 ? (
            <div className="edit">
                <button className="btn-edit"
                onClick={() => handleEdit(item.id)}>

              <img
                
                src="./images/icon-edit.svg"
                alt=""
                />
                <span>Edit</span>
                </button>
                <button className="btn-delete" onClick={() => {
                    handleModal(item.id)
                    setEdit({edit: false});
                }}>

              <img
                
                src="./images/icon-delete.svg"
                alt=""
                />
                <span>Delete</span>
                </button>
            </div>
          ) : null}
        </div>
        <div className="content">
          <p suppressContentEditableWarning={isUser} contentEditable={isUser}>
            {item.comment}
          </p>
        </div>
        {isUser && (
          <button className="btn-update" onClick={() => setEdit(false)}>Update</button>
        )}
      </div>
    </div>
  );
};

export default Comment;
