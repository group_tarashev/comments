import React, { Fragment, useEffect, useState } from "react";
import Comment from "./Comment";
import "../styles/comments.css";
import Input from "./Input";

const CommentList = ({
  userId,
  user,
  data,
  handleInput,
  handleSubmit,
  replay,
  isLoading,
  handleDelete,
  handleEdit,
  edit,
  setEdit,
  handleLikePlus,
  handleLikeMinus,
  handleModal
}) => {
  // const {data, isLoading} =  useQuery('get-comments', request)

  if (isLoading) {
    return <h2>Loading...</h2>;
  }
  return (
    <div className="comments-outer">
      <h2>Comments List</h2>
      <ul className="comments">
        {data?.map((item, i) => (
          <Fragment key={i}>
            {item && <Comment
              item={item}
              repay={replay}
              user={user}
              handleDelete={handleDelete}
              handleEdit={handleEdit}
              edit={edit}
              setEdit={setEdit}
              handleLikePlus={handleLikePlus}
              handleLikeMinus={handleLikeMinus}
              handleModal={handleModal}
            />}
            {item && item.id === userId && (
              <Input
                index={i}
                handleInput={handleInput}
                handleSubmit={handleSubmit}
                user={user}
                replay={replay}
              />
            )}
          </Fragment>
        ))}
      </ul>
    </div>
  );
};

export default CommentList;
