import React, { useState } from "react";
import "../styles/input.css";
const Input = (props) => {
    const { user, handleInput, handleSubmit, index, value } = props
    
  return (
    <div className="input-comment">
      <div className="avatar">
        <img src={user.img} alt="" />
      </div>
      <form action="" onSubmit={(e) => handleSubmit(e, index)}>
        <textarea
          name={user.user1}
          className="textarea"
          placeholder="Add comment..."
          rows={4}
          cols={35}
          type="text"
          value={value}
          height={100}
          onChange={(e) => handleInput(e,user.img)}
        />
        <button className="btn-send">Send</button>
      </form>
    </div>
  );
};

export default Input;
